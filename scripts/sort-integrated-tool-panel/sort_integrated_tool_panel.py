import argparse
import sys
import collections
import xml.etree.ElementTree as ElementTree
from yaml import safe_load, YAMLError

def indent(elem, level=0):
    i = "\n\n" + level*"  "
    j = "\n\n" + (level-1)*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for subelem in elem:
            indent(subelem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = j
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = j
    return elem   

def tool_version(xml_id):
    return xml_id.split('/')[-1]

def tool_xml_name(xml_id):
    return xml_id.split('/')[-2]

def tool_name(xml_id):
    return xml_id.split('/')[-3]

def tool_owner(xml_id):
    return xml_id.split('/')[-4]

def get_tool_id(xml_id):
    return tool_owner(xml_id)+"_"+tool_name(xml_id)

def sort_tools_from_section(section,yaml_ids):

    #set up ordered dictionary of tools for a section 
    tools = collections.OrderedDict()
    for tool in section.findall('tool'):
        tool_id = get_tool_id(tool.get('id'))
        if tool_id not in tools:
            tools[tool_id] = []
        tools[tool_id].append(tool)

    #create empty section
    sorted_section = ElementTree.Element('section')
    sorted_section.attrib = section.attrib

    #add tool for each yaml_id
    for yaml_id in yaml_ids:
        if yaml_id in tools:
            for tool in tools[yaml_id]:
                sorted_section.append(tool)

    #add installed tools not in yaml_ids
    for tool_id in tools:
        if tool_id not in yaml_ids:
            for tool in tools[tool_id]:
                sorted_section.append(tool)

    return sorted_section

def main(args):
    print('Reading integrated_tool_panel.xml ...')
    tree = ElementTree.parse(args.integrated_tool_panel.name)
    #tree = ElementTree.parse('/home/dchristiany/integrated_tool_panel.xml')
    toolbox = tree.getroot()

    tool_panel_section_id = {}
    for yaml in args.yaml:
        with open(yaml.name,'r') as stream:
            try:
                tools_list  = safe_load(stream)['tools']
            except YAMLError as exc:
                print(exc)

        #set up tool_panel_section_id dictionary with list of tools for each section
        for tool in tools_list:
            tool_id = tool['owner']+'_'+tool['name']
            section_id = tool['tool_panel_section_id']
            if section_id not in tool_panel_section_id:
                tool_panel_section_id[section_id] = []
            tool_panel_section_id[section_id].append(tool_id)

    #sort section if referenced in 'tool_panel_section_id' dictionary in a new tree
    sorted_toolbox = ElementTree.Element('toolbox')
    for element in toolbox:
        if element.tag == 'section' and element.get('id') in tool_panel_section_id:
                yaml_ids = tool_panel_section_id[element.get('id')]
                sorted_section = sort_tools_from_section(element,yaml_ids)
                sorted_toolbox.append(sorted_section)
        else:
            sorted_toolbox.append(element)

    print('XML Writing ...')
    xml_string = ElementTree.tostring(indent(sorted_toolbox)).decode()
    output_file = open(args.integrated_tool_panel.name+".sorted.xml", "w")
    output_file.write(xml_string)
    output_file.close()

    print('Cheking ...')
    print(args.integrated_tool_panel.name+".sorted.xml")
    tree_new_imported = ElementTree.parse(args.integrated_tool_panel.name+".sorted.xml")
    toolbox_new_imported = tree_new_imported.getroot()
    if len(toolbox_new_imported.findall('section/tool')) != len(toolbox.findall('section/tool')):
        sys.stderr.write("The number of tools in the two integrated_tool_panel didn't match - org:%d != new:%d \n" % (len(toolbox_new_imported.findall('section/tool')), len(toolbox.findall('section/tool'))))
        sys.exit(2)

if __name__ == "__main__":
    # execute only if run as a script

    parser = argparse.ArgumentParser(description='This tool will reorder the integrated_tool_panel.xml according to yaml tool list')
    parser.add_argument('--integrated_tool_panel', type=argparse.FileType('r'), required=True, help="integrated_tool_panel.xml file")
    parser.add_argument('--yaml', type=argparse.FileType('r'), nargs='+', required=True, help="tool.yaml file[s]")
    args = parser.parse_args()

    main(args)